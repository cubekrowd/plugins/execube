/*

    execube
    Copyright (C) 2018  CubeKrowd Network

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package net.cubekrowd.execube.common;

import java.util.*;
import lombok.Data;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

@Data
public class ECConfiguration {
    private final Map<String, List<String>> groups;
    private final String redisHost;
    private final int redisPort;
    private final String redisPassword;
    private final String serverID;

    public String[] getRedisChannels() {
        Set<String> channels = new HashSet<>();
        channels.add("ec-all");
        channels.add("ec-" + serverID);
        groups.values().forEach(l -> l.forEach(k -> channels.add("ec-" + k)));
        return channels.toArray(new String[0]);
    }

    public JedisPool createJedisPool() {
        if (redisPassword == null || redisPassword.isEmpty()) {
            return new JedisPool(new JedisPoolConfig(), redisHost, redisPort, 0);
        } else {
            return new JedisPool(new JedisPoolConfig(), redisHost, redisPort, 0, redisPassword);
        }
    }
}
