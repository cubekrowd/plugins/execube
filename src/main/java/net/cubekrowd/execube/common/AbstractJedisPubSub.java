/*

    execube
    Copyright (C) 2018  CubeKrowd Network

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package net.cubekrowd.execube.common;

import java.util.function.*;
import lombok.RequiredArgsConstructor;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPubSub;
import redis.clients.jedis.exceptions.JedisConnectionException;

@RequiredArgsConstructor
public abstract class AbstractJedisPubSub extends JedisPubSub implements Runnable {
    private final JedisPool pool;
    private final ECConfiguration configuration;
    private final Consumer<AbstractJedisPubSub> runSub;

    @Override
    public void run() {
        try (Jedis jedis = pool.getResource()) {
            jedis.subscribe(this, configuration.getRedisChannels());
        } catch (JedisConnectionException e) {
            runSub.accept(this);
            e.printStackTrace();
        }
    }

    @Override
    public void onPMessage(String s, String s1, String s2) {
    }

    @Override
    public void onSubscribe(String s, int i) {
    }

    @Override
    public void onUnsubscribe(String s, int i) {
    }

    @Override
    public void onPUnsubscribe(String s, int i) {
    }

    @Override
    public void onPSubscribe(String s, int i) {
    }
}
