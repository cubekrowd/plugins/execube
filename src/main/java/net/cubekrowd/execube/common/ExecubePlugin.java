/*

    execube
    Copyright (C) 2018  CubeKrowd Network

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package net.cubekrowd.execube.common;

import java.util.*;
import net.md_5.bungee.api.*;
import net.md_5.bungee.api.chat.*;

public interface ExecubePlugin {

    ComponentBuilder PREFIX = new ComponentBuilder("[").color(ChatColor.DARK_GRAY).append("Execube").color(ChatColor.DARK_GREEN).append("] ").color(ChatColor.DARK_GRAY);

    // Send messages
    BaseComponent[] SEND_SUCCESS = new ComponentBuilder(PREFIX).append("Command successfully queued for execution.").color(ChatColor.GREEN).create();
    BaseComponent[] SEND_FAILED = new ComponentBuilder(PREFIX).append("Could not send the command! Please try again.").color(ChatColor.RED).create();

    // Help menu
    BaseComponent[] INVALID_COMMAND = new ComponentBuilder(PREFIX).append("Invalid command!").color(ChatColor.RED).create();
    BaseComponent[] VERSION = new ComponentBuilder(PREFIX).append("Running version ").color(ChatColor.DARK_GREEN).create();
    BaseComponent[] HELP_1 = new ComponentBuilder(PREFIX).append("/execube ").color(ChatColor.DARK_AQUA).append("help ").color(ChatColor.GOLD).create();
    BaseComponent[] HELP_2 = new ComponentBuilder(PREFIX).append("/execube ").color(ChatColor.DARK_AQUA).append("reload ").color(ChatColor.GOLD).create();
    BaseComponent[] HELP_3 = new ComponentBuilder(PREFIX).append("/execube ").color(ChatColor.DARK_AQUA).append("<group/server> ").color(ChatColor.GOLD).append("<command>").color(ChatColor.AQUA).create();
    BaseComponent[] HELP_4 = new ComponentBuilder(PREFIX).append("/execube ").color(ChatColor.DARK_AQUA).append("<bungee/spigot/all> ").color(ChatColor.GOLD).append("<command>").color(ChatColor.AQUA).create();
    BaseComponent[] HELP_5 = new ComponentBuilder(PREFIX).append("/execube ").color(ChatColor.DARK_AQUA).append("credits ").color(ChatColor.GOLD).create();
    List<BaseComponent[]> HELP = Arrays.asList(HELP_1, HELP_2, HELP_3, HELP_4, HELP_5);

    // credits
    BaseComponent[] CREDITS = new ComponentBuilder(PREFIX).append("Credits:").color(ChatColor.DARK_GREEN).create();
    BaseComponent[] CRE_AUTHORS = new ComponentBuilder(PREFIX).append("Authors: ").color(ChatColor.DARK_AQUA).create();
    BaseComponent[] CRE_VERSION = new ComponentBuilder(PREFIX).append("Version: ").color(ChatColor.DARK_AQUA).create();
    BaseComponent[] CRE_SOURCE_CODE = new ComponentBuilder(PREFIX).append("Source Code: ").color(ChatColor.DARK_AQUA).create();
    BaseComponent[] CRE_THANK_YOU = new ComponentBuilder(PREFIX).append("This is a free open-source plugin, originally developed for CubeKrowd. If you like it then please leave a review on the Spigot page. Thank you.").color(ChatColor.AQUA).create();

    // config
    BaseComponent[] RELOADED = new ComponentBuilder(PREFIX).append("Reloaded configuration.").color(ChatColor.GREEN).create();

    default String stripSlash(String s) {
        if(s.startsWith("/")) {
            return s.substring(1);
        } else {
            return s;
        }
    }

}
