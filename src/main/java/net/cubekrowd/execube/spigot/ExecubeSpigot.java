/*

    execube
    Copyright (C) 2018  CubeKrowd Network

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package net.cubekrowd.execube.spigot;

import java.util.*;
import java.util.function.*;
import java.util.stream.*;
import net.cubekrowd.execube.common.*;
import org.bukkit.configuration.*;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.exceptions.JedisConnectionException;

import lombok.*;

public class ExecubeSpigot extends JavaPlugin implements ExecubePlugin {

    @Getter private JedisPool pool;
    @Getter private ECConfiguration configuration;
    private ECSubscriber ecSubscriber;

    @Override
    public void onEnable() {
        saveDefaultConfig();
        loadConfig();

        getCommand("execube").setExecutor(new ECCommand(this));
    }

    @Override
    public void onDisable() {
        ecSubscriber.unsubscribe();
        pool.destroy();
    }

    public void loadConfig() {
        reloadConfig();

        String ip = getConfig().getString("ip", "127.0.0.1");
        int port = getConfig().getInt("port", 6379);
        String password = getConfig().getString("password", "");
        String serverID = getConfig().getString("server-id", "spigot");
        ConfigurationSection groupSection = getConfig().getConfigurationSection("groups");
        Map<String, List<String>> groups = new HashMap<>();
        groups.put("bungee", Arrays.asList("bungeecord"));
        groups.put("bungeecord", Arrays.asList("bungeecord"));
        groups.put("bukkit", Arrays.asList("spigot"));
        groups.put("spigot", Arrays.asList("spigot"));
        groupSection.getKeys(false).forEach(k -> groups.put(k, Arrays.asList(groupSection.getString(k).split(",")).stream().map(s -> s.toLowerCase()).collect(Collectors.toList())));
        configuration = new ECConfiguration(groups, ip, port, password, serverID);

        // @TODO Destroy old pool.
        pool = configuration.createJedisPool();
        if(ecSubscriber != null) {
            try {
                ecSubscriber.unsubscribe();
            } catch(JedisConnectionException e) {
                // do nothing
            }
            ecSubscriber = null;
        }
        getServer().getScheduler().cancelTasks(this); // cancel the old task
        // @TODO Running this asynchronously probably causes all kinds of
        // thread-safety issues, but running it synchronously will lock the
        // server, because Jedis' subscribe method halts the thread it's called
        // on. We should really make this all thread-safe. The same is true for
        // the BungeeCord side, although we don't have the option to run it
        // synchronously there.
        Consumer<AbstractJedisPubSub> runSub = (AbstractJedisPubSub a) -> getServer().getScheduler().runTaskLaterAsynchronously(ExecubeSpigot.this, a, 5);
        runSub.accept(ecSubscriber = new ECSubscriber(runSub));
    }

    public class ECSubscriber extends AbstractJedisPubSub {
        public ECSubscriber(Consumer<AbstractJedisPubSub> runSub) {
            super(pool, configuration, runSub);
        }

        @Override
        public void onMessage(String channel, final String msg) {
            if(!channel.equals("ec-all") && !channel.equals("ec-" + configuration.getServerID()) && !channel.equals("ec-spigot")) {
                return;
            }
            // Needs to be done in the server thread
            new BukkitRunnable() {
                @Override
                public void run() {
                    getLogger().info("Dispatching /" + msg);
                    getServer().dispatchCommand(getServer().getConsoleSender(), msg);
                }
            } .runTask(ExecubeSpigot.this);
        }
    }
}

