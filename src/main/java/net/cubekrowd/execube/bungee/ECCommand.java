/*

    execube
    Copyright (C) 2018  CubeKrowd Network

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package net.cubekrowd.execube.bungee;

import java.util.*;
import java.util.function.*;
import java.util.stream.*;
import net.md_5.bungee.api.*;
import net.md_5.bungee.api.chat.*;
import net.md_5.bungee.api.plugin.Command;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.exceptions.JedisConnectionException;

public class ECCommand extends Command {

    private final ExecubeBungee plugin;

    public ECCommand(ExecubeBungee plugin) {
        super("execube", null, "exec", "execute");
        this.plugin = plugin;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        Consumer<CommandSender> printVersion = s -> s.sendMessage(Stream.of(plugin.VERSION, TextComponent.fromLegacyText(ChatColor.DARK_AQUA + plugin.getDescription().getVersion())).flatMap(Stream::of).toArray(BaseComponent[]::new));
        Consumer<CommandSender> printHelp = s -> plugin.HELP.forEach(part -> s.sendMessage(part));

        // print help
        if(args.length == 0 || args[0].equalsIgnoreCase("help")) {
            printVersion.accept(sender);
            if (sender.hasPermission("execube.use")) {
                printHelp.accept(sender);
            }
            return;
        }

        // credits sub-command
        if (args[0].equalsIgnoreCase("credits")) {
            sender.sendMessage(plugin.CREDITS);
            sender.sendMessage(Stream.of(plugin.CRE_AUTHORS, TextComponent.fromLegacyText(ChatColor.GOLD + plugin.getDescription().getAuthor())).flatMap(Stream::of).toArray(BaseComponent[]::new));
            sender.sendMessage(Stream.of(plugin.CRE_VERSION, TextComponent.fromLegacyText(ChatColor.GOLD + plugin.getDescription().getVersion())).flatMap(Stream::of).toArray(BaseComponent[]::new));
            sender.sendMessage(Stream.of(plugin.CRE_SOURCE_CODE, TextComponent.fromLegacyText(ChatColor.GOLD + "https://gitlab.com/cubekrowd/" + plugin.getDescription().getName().toLowerCase())).flatMap(Stream::of).toArray(BaseComponent[]::new));
            sender.sendMessage(plugin.CRE_THANK_YOU);
            return;
        }

        // check perms
        if (!sender.hasPermission("execube.use")) {
            printVersion.accept(sender);
            return;
        }

        // reload sub-command
        if (args[0].equalsIgnoreCase("reload")) {
            plugin.reloadFromConfig();
            sender.sendMessage(plugin.RELOADED);
            return;
        }

        // check args length
        if(args.length == 1) {
            sender.sendMessage(plugin.INVALID_COMMAND);
            return;
        }

        List<String> channels = new ArrayList<>();
        String run = plugin.stripSlash(String.join(" ", Arrays.copyOfRange(args, 1, args.length)));

        for(String key : args[0].split(",")) {
            Map.Entry<String, List<String>> group = plugin.getConfiguration().getGroups().entrySet().stream().filter(e -> e.getKey().equals(key.toLowerCase())).findFirst().orElse(null);
            if(group == null) {
                channels.add(key.toLowerCase());
            } else {
                channels.addAll(group.getValue());
            }
        }

        plugin.getProxy().getScheduler().runAsync(plugin, new Runnable() {
            @Override
            public void run() {
                try (Jedis jedis = plugin.getPool().getResource()) {
                    channels.forEach(c -> jedis.publish("ec-" + c, run));
                    sender.sendMessage(plugin.SEND_SUCCESS);
                } catch (JedisConnectionException e) {
                    sender.sendMessage(plugin.SEND_FAILED);
                }
            }
        });
    }
}
