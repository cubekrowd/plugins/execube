/*

    execube
    Copyright (C) 2018  CubeKrowd Network

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package net.cubekrowd.execube.bungee;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.stream.Collectors;
import lombok.Getter;
import net.cubekrowd.execube.common.AbstractJedisPubSub;
import net.cubekrowd.execube.common.ECConfiguration;
import net.cubekrowd.execube.common.ExecubePlugin;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.exceptions.JedisConnectionException;

public class ExecubeBungee extends Plugin implements ExecubePlugin {

    @Getter private JedisPool pool;
    @Getter private ECConfiguration configuration;
    private ECSubscriber ecSubscriber = null;

    public void saveDefaultConfig(String name) {
        getDataFolder().mkdirs();
        var configFile = new File(getDataFolder(), name);
        try {
            Files.copy(getResourceAsStream(name), configFile.toPath());
        } catch (FileAlreadyExistsException e) {
            // ignore
        } catch (IOException e) {
            getLogger().log(Level.WARNING, "Can't save default config " + name, e);
        }
    }

    public Configuration loadConfig(String name) {
        var configProvider = ConfigurationProvider.getProvider(YamlConfiguration.class);
        try {
            return configProvider.load(new File(getDataFolder(), name));
        } catch (IOException e) {
            getLogger().log(Level.WARNING, "Can't load config " + name, e);
            return new Configuration();
        }
    }

    @Override
    public void onEnable() {
        saveDefaultConfig("config.yml");
        reloadFromConfig();

        getProxy().getPluginManager().registerCommand(this, new ECCommand(this));
    }

    @Override
    public void onDisable() {
        try {
            ecSubscriber.unsubscribe();
        } catch(JedisConnectionException e) {
            e.printStackTrace();
        }
        pool.destroy();
    }

    public void reloadFromConfig() {
        var config = loadConfig("config.yml");
        String ip = config.getString("ip", "127.0.0.1");
        int port = config.getInt("port", 6379);
        String password = config.getString("password", "");
        String serverID = config.getString("server-id", "spigot");
        Configuration groupSection = config.getSection("groups");
        Map<String, List<String>> groups = new HashMap<>();
        groups.put("bungee", Arrays.asList("bungeecord"));
        groups.put("bungeecord", Arrays.asList("bungeecord"));
        groups.put("bukkit", Arrays.asList("spigot"));
        groups.put("spigot", Arrays.asList("spigot"));
        groupSection.getKeys().forEach(k -> groups.put(k, Arrays.asList(groupSection.getString(k).split(",")).stream().map(s -> s.toLowerCase()).collect(Collectors.toList())));
        configuration = new ECConfiguration(groups, ip, port, password, serverID);

        pool = configuration.createJedisPool();
        if(ecSubscriber != null) {
            try {
                ecSubscriber.unsubscribe();
            } catch(JedisConnectionException e) {
                e.printStackTrace();
            }
            ecSubscriber = null;
        }
        getProxy().getScheduler().cancel(this); // cancel the old task
        Consumer<AbstractJedisPubSub> runSub = (AbstractJedisPubSub a) -> getProxy().getScheduler().schedule(ExecubeBungee.this, a, 250, TimeUnit.MILLISECONDS);
        runSub.accept(ecSubscriber = new ECSubscriber(runSub));
    }

    public class ECSubscriber extends AbstractJedisPubSub {
        public ECSubscriber(Consumer<AbstractJedisPubSub> runSub) {
            super(pool, configuration, runSub);
        }

        @Override
        public void onMessage(String channel, final String msg) {
            if(!channel.equals("ec-all") && !channel.equals("ec-" + configuration.getServerID()) && !channel.equals("ec-bungeecord")) {
                return;
            }
            getLogger().info("Dispatching /" + msg);
            getProxy().getPluginManager().dispatchCommand(getProxy().getConsole(), msg);
        }
    }

}

