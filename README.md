# Execube
![Execube](https://assets.gitlab-static.net/cubekrowd/execube/raw/master/docs/logotext.png "Execube")  

**NOTE: JAVA 10 IS REQUIRED FOR THIS PLUGIN. IF YOU ARE NOT RUNNING JAVA 10 THEN PLEASE UPGRADE**

Execube is plugin which works on both BungeeCord and Spigot, and enables you to remotely execute commands on other servers even if no players are online. Servers can be categorised into groups for easier execution across a range of servers (for example if you have multiple grouped servers like minigames). The plugin is very light-weight and uses Redis for communication. This plugin is an updated fork of [ExecuteEverywhere](https://www.spigotmc.org/resources/executeeverywhere.524/ "ExecuteEverywhere") to add more features such as the ability to execute a command on a specific server.

### [Bugs & Feature Requests](https://gitlab.com/cubekrowd/execube/issues "Bugs & Feature Requests")

## Features

 * Execute commands on other servers
 * Execute commands on servers even if no players are online on that server
 * Supports both Spigot and BungeeCord (however neither are required for the other to work)
 * Works and tested with multiple BungeeCords
 * Ability to categorize servers into groups for easier execution
 * Uses Redis for reliable and ultra-quick communication

## Installation

 1. Drop the plugin into the plugin folder on all servers.
 2. Install a Redis server. Remember to properly firewall port 6379 and set a Redis password. Preferred is if you search online for the appropriate tutorial for your operating sytem. Installing Redis on Debian/Ubuntu can be done with the following commands:
    1. `sudo apt-get install redis-server`
    2. `sudo nano /etc/redis/redis.conf`
    3.  Scroll down to the SECURITY section, uncomment `requirepass` and change the password to something really secure and LONG. Mash the keyboard a bit. Copy the same password into the plugin config.
    4. `sudo systemctl restart redis-server.service`
 3. Set a unique server name in the config for each server.
 4. Restart the server for the plugin to load.

## Configuration

The following groups: `bungee`, `bungeecord`, `spigot`, `bukkit` and `all` are automatically registered. 
The keywords `help` and `credits` cannot be used as group names. Please see the below example for usage:

```yml
# Execube config
#
# Redis connection info
ip: 127.0.0.1
port: 6379
password: ""
#
# Unique ID for this server
server-id: "bungeecord2"
#
# Create groups where a command will be executed on all
groups:
  test: "creative,survival"
```

## Commands

Servers can also be comma-separated, however, defining a group in the config is recommended for increased user-experience. If a server is not found on the network then it will simply skip that server.

 * `/execube help` - Prints the help menu  
 * `/execube reload` - Reloads the configuration  
 * `/execube <group/server> <command>` - Executes a command on another server/servers  
 * `/execube credits` - Prints plugin information  

## Permissions

This plugin only has one predefined permission node by default, which is:  

`execube.use` - Allows access to the /execube command and execute commands on other servers.

## Credits & Support

This plugin is Open-Source, released under AGPL-3.0.
If you want to contribute you can find the repository at:  

[GitLab](https://gitlab.com/cubekrowd/execube "[GitLab]")
